# Template file for 'openbox'
pkgname=openbox
version=3.4.10
distfiles="http://icculus.org/openbox/releases/$pkgname-$version.tar.gz"
build_style=gnu_configure
configure_args="--enable-startup-notification"
short_desc="Standards compliant, fast, light-weight, extensible window manager"
maintainer="Juan RP <xtraeme@gmail.com>"
checksum=20356f93c3334c3209284fba2628a9c48c3140a35febd0bdafa3a164d0eaed00
long_desc="
 Openbox works with your applications, and makes your desktop easier to
 manage. This is because the approach to its development was the opposite of
 what seems to be the general case for window managers. Openbox was written
 first to comply with standards and to work properly. Only when that was in
 place did the team turn to the visual interface.

 Openbox is fully functional as a stand-alone working environment, or can be
 used as a drop-in replacement for the default window manager in the GNOME
 or KDE desktop environments.

 Some of the things to look for in Openbox are:

  * ICCCM and EWMH compliance!
  * Very fast
  * Chainable key bindings
  * Customizable mouse actions
  * Window resistance
  * Multi-head Xinerama support!
  * Pipe menus"

conf_files="
/etc/xdg/openbox/menu.xml
/etc/xdg/openbox/rc.xml
/etc/xdg/openbox/autostart.sh"

Add_dependency build pkg-config
Add_dependency run glibc

_deps="fontconfig freetype glib pango libICE libSM libX11 libXau libXcursor"
_deps="${_deps} libXext libXft libXinerama libXrandr libXrender libxml2"
_deps="${_deps} startup-notification"

for _dep_ in ${_deps}; do
	Add_dependency run	${_dep_}
	Add_dependency build	${_dep_}-devel
done
